# Test Environment

This git has the objective to provide a test environment for Ansible with 
vagrant.

## Requirements

* Ansible
* Vagrant
* VirtualBox

## Usage

For the first time, use:

`vagrant up provisional`

This will create a virtual machine with Centos7 (you can modify all the 
preferences in the vagrantfile) and two network interfaces (NAT and HostOnly),
this machine will be provided with ansible.

If you change something in your ansible run:

`vagrant rsync provisional`

`vagrant provision provisional`